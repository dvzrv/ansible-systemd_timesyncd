<!--
SPDX-FileCopyrightText: 2023 David Runge <dvzrv@archlinux.org>
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# archlinux.systemd_timesyncd

An [Ansible](https://www.ansible.com/) role to configure
[systemd-timesyncd](https://www.freedesktop.org/software/systemd/man/systemd-timesyncd.service.html)
via a configuration dropin (see [man
timsyncd.conf](https://www.freedesktop.org/software/systemd/man/timesyncd.conf.html)
for further information) to setup [Network Time Protocol
 (NTP)](https://en.wikipedia.org/wiki/Network_Time_Protocol) synchronization
 for a Linux machine. Additionally it is able to set the [Real-time Clock
 (RTC)](https://en.wikipedia.org/wiki/Real-time_clock) to [Coordinated
 Universal Time (UTC)](https://en.wikipedia.org/wiki/Coordinated_Universal_Time).

## Requirements

An Arch Linux system or an Arch Linux chroot.

## Role Variables

* `systemd_timesyncd`: a dict describing the configuration for systemd-timesyncd (defaults to unset)
  * `timezone`: a string defining the timezone (defaults to `UTC`)
  * `rtc_localtime`: a boolean value indicating whether the RTC should be set to be in localtime (UTC otherwise) (defaults to `false`)
  * `ntp`: a list of strings defining which NTP servers to use (defaults to `[]`)
  * `fallbackntp`: a list of strings defining which NTP servers to use as fallback (defaults to `['0.arch.pool.ntp.org', '1.arch.pool.ntp.org', '2.arch.pool.ntp.org', '3.arch.pool.ntp.org']`)
  * `rootdistancemaxsec`: a non-negative number in seconds, describing the maximum acceptable root distance (defaults to `5`)
  * `pollintervalminsec`: a non-negative number in seconds, describing the minimum poll interval (defaults to `32`)
  * `pollintervalmaxsec`: a non-negative number in seconds, describing the maximum poll interval (defaults to `2048`)
  * `connectionretrysec`: a non-negative number in seconds, describing the delay before subsequent attempts to contact a new NTP server (defaults to `30`)
  * `saveintervalsec`: a non-negative number in seconds, describing the interval at which the current time is periodically saved to disk (defaults to `60`)
  * `service`: a boolean value indicating whether to enable (and start) `systemd-timesyncd.service` (defaults to `true`)

The following variables have no defaults in this role, but are available to override behavior and use special functionality:

* `chroot_dir`: the directory to chroot into, when including `chroot.yml` tasks (defaults to unset)

## Dependencies

None

## Example Playbook

```yaml
- name: Set a custom timezone
  hosts: my_hosts
  vars:
    systemd_timesyncd:
      timezone: Europe/Berlin
  tasks:
    - name: Include archlinux.systemd_timesyncd
      ansible.builtin.include_role:
        name: archlinux.systemd_timesyncd
```

## License

GPL-3.0-or-later

## Author Information

David Runge <dvzrv@archlinux.org>
